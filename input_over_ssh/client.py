#   input-over-ssh: Forwarding arbitrary input devices over SSH
#   Copyright © 2019  Lee Yingtong Li (RunasSudo)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

PROTOCOL_VERSION = '2'

import argparse
import asyncio
import evdev
import json
import sys


async def do_forward_device(i, device):
    global active

    pressed_keys = []
    grabbed = False
    switch_done = False

    if args.exclusive:
        device.grab()
        grabbed = True

    async for event in device.async_read_loop():
        if event.type == 1:
            if event.value == 1:
                pressed_keys.append(evdev.events.KEY.get(event.code))
            elif event.value == 0 and evdev.events.KEY.get(event.code) in pressed_keys:
                pressed_keys.remove(evdev.events.KEY.get(event.code))

        if active:
            if args.exclusive and not grabbed and len(pressed_keys) == 0:
                device.grab()
                grabbed = True

            print(json.dumps([i, event.type, event.code, event.value]))
        elif args.exclusive and grabbed and len(pressed_keys) == 0:
            device.ungrab()
            grabbed = False

        if args.switch_keys and len(pressed_keys) == len(args.switch_keys.split(',')):
            for key in args.switch_keys.split(','):
                if key not in pressed_keys:
                    switch_done = False

                    break
            else:
                if not switch_done:
                    active = not active
                    switch_done = True
        else:
            switch_done = False

        if args.debug:
            print('pressed keys:', pressed_keys, 'active:', active, 'graabbed:', grabbed, file=sys.stderr)

async def forward_device(i, device):
    await do_forward_device(i, device)

def encode_device(device):
    cap = device.capabilities()
    del cap[0]  # Filter out EV_SYN, otherwise we get OSError 22 Invalid argument
    cap_json = {}
    for k, v in cap.items():
        cap_json[k] = [x if not isinstance(x, tuple) else [x[0], x[1]._asdict()] for x in v]
    return {'name': device.name, 'capabilities': cap_json, 'vendor': device.info.vendor, 'product': device.info.product}

async def run_forward():
    # Find devices
    if args.all:
        devices = []
        for path in evdev.list_devices():
            device = evdev.InputDevice(path)
            devices.append(device)
    else:
        devices_by_name = {}
        if args.device_by_name:
            for path in evdev.list_devices():
                device = evdev.InputDevice(path)
                devices_by_name[device.name] = device

        devices = []
        for path in args.device_by_path:
            devices.append(evdev.InputDevice(path))
        for name in args.device_by_name:
            devices.append(devices_by_name[name])

    # Report version
    print(PROTOCOL_VERSION)

    # Report devices
    print(json.dumps([encode_device(device) for device in devices]))

    tasks = []
    for i, device in enumerate(devices):
        tasks.append(asyncio.create_task(forward_device(i, device)))

    try:
        await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
    except KeyboardInterrupt:
        for device in devices:
            device.ungrab()

async def list_devices():
    for path in evdev.list_devices():
        device = evdev.InputDevice(path)
        print('{}  {}'.format(device.path, device.name))

parser = argparse.ArgumentParser(description='input-over-ssh client')
parser.add_argument('-L', '--list-devices', dest='action', action='store_const', const=list_devices, help='List available input devices')
parser.add_argument('-p', '--device-by-path', action='append', default=[], help='Forward device with the given path')
parser.add_argument('-n', '--device-by-name', action='append', default=[], help='Forward device with the given name')
parser.add_argument('-a', '--all', action='store_true', help='Forward all the devices')
parser.add_argument('-e', '--exclusive', action='store_true', help='Grab the device for exclusive input')
parser.add_argument('-s', '--start-disabled', action='store_true', help='Needs the switch key combination before starting')
parser.add_argument('-d', '--debug', action='store_true', help='Debug mode (prints pressed keys)')
parser.add_argument('-k', '--switch-keys', type=str, default='', help='Define key combination to enable/disable forwarding. Example: "KEY_LEFTMETA,KEY_LEFTSHIFT,KEY_S"')
args = parser.parse_args()

if not args.action:
    args.action = run_forward

active = not args.start_disabled

asyncio.run(args.action())
